{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "edu-ciaa-fpga",
    "graph": {
      "blocks": [
        {
          "id": "64bb1c49-3c0e-47f6-a6c7-421ce5049e3b",
          "type": "basic.input",
          "data": {
            "name": "CLK",
            "pins": [
              {
                "index": "0",
                "name": "CLK",
                "value": "94"
              }
            ],
            "virtual": false,
            "clock": false
          },
          "position": {
            "x": -776,
            "y": 200
          }
        },
        {
          "id": "74a6cbb9-5a65-4365-824c-c8d0be5cfab7",
          "type": "basic.output",
          "data": {
            "name": "",
            "pins": [
              {
                "index": "0",
                "name": "LED0",
                "value": "1"
              }
            ],
            "virtual": false
          },
          "position": {
            "x": 80,
            "y": 280
          }
        },
        {
          "id": "d8f7e180-2cbc-4790-b754-493f7414d28c",
          "type": "basic.constant",
          "data": {
            "name": "Prescale",
            "value": "16",
            "local": false
          },
          "position": {
            "x": -568,
            "y": 96
          }
        },
        {
          "id": "5aafa498-0063-41c6-961d-b11ecd312222",
          "type": "basic.constant",
          "data": {
            "name": "Umbral",
            "value": "120",
            "local": false
          },
          "position": {
            "x": -176,
            "y": 96
          }
        },
        {
          "id": "6f5b6b52-c1aa-4148-880d-b9f41f1fdaa8",
          "type": "6a50747141af6d1cfb3bb9d0093fb94862ff5a65",
          "position": {
            "x": -568,
            "y": 200
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "1441dc7a-d30b-4f4c-97e0-33ba979c192a",
          "type": "82041acc0f349dd5f038765c7d783bbe4b20dc00",
          "position": {
            "x": -568,
            "y": 280
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "6ad76801-b927-48bc-a416-f170888affca",
          "type": "82041acc0f349dd5f038765c7d783bbe4b20dc00",
          "position": {
            "x": -568,
            "y": 360
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "21bad408-76bc-4c00-9d44-1110f1e11410",
          "type": "82041acc0f349dd5f038765c7d783bbe4b20dc00",
          "position": {
            "x": -568,
            "y": 440
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "febb5024-8706-4dd6-a645-27945e6ae082",
          "type": "82041acc0f349dd5f038765c7d783bbe4b20dc00",
          "position": {
            "x": -568,
            "y": 520
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "bddf1f80-656c-4d50-9fc2-08652f6c5794",
          "type": "82041acc0f349dd5f038765c7d783bbe4b20dc00",
          "position": {
            "x": -568,
            "y": 600
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "9478ff14-6e19-42b9-8033-d9aab0312701",
          "type": "82041acc0f349dd5f038765c7d783bbe4b20dc00",
          "position": {
            "x": -568,
            "y": 680
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "af952fcf-95a9-4bfe-901a-5744fce54d91",
          "type": "82041acc0f349dd5f038765c7d783bbe4b20dc00",
          "position": {
            "x": -568,
            "y": 760
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "1edb8255-0871-4c81-8288-50d9d96b5f73",
          "type": "basic.code",
          "data": {
            "code": "wire [7:0] q;\n\nassign q[0] = a0;\nassign q[1] = a1;\nassign q[2] = a2;\nassign q[3] = a3;\nassign q[4] = a4;\nassign q[5] = a5;\nassign q[6] = a6;\nassign q[7] = a7;\n\nassign cmp = (q<N);",
            "params": [
              {
                "name": "N"
              }
            ],
            "ports": {
              "in": [
                {
                  "name": "a0"
                },
                {
                  "name": "a1"
                },
                {
                  "name": "a2"
                },
                {
                  "name": "a3"
                },
                {
                  "name": "a4"
                },
                {
                  "name": "a5"
                },
                {
                  "name": "a6"
                },
                {
                  "name": "a7"
                }
              ],
              "out": [
                {
                  "name": "cmp"
                }
              ]
            }
          },
          "position": {
            "x": -296,
            "y": 216
          },
          "size": {
            "width": 328,
            "height": 192
          }
        },
        {
          "id": "a56e8cc2-af14-4f3e-89a2-49b635f475c9",
          "type": "basic.info",
          "data": {
            "info": "## EJEMPLO 3 EDU-CIAA-FPGA:<br>Modulacion de ancho de pulsos\nUsando la entrada de clock de 12 MHz, un prescaler y un comparador en Verilog",
            "readonly": true
          },
          "position": {
            "x": -280,
            "y": 448
          },
          "size": {
            "width": 352,
            "height": 64
          }
        },
        {
          "id": "4b575886-8848-4340-892f-08ecce8996c6",
          "type": "basic.info",
          "data": {
            "info": "12 MHz",
            "readonly": true
          },
          "position": {
            "x": -656,
            "y": 208
          },
          "size": {
            "width": 136,
            "height": 64
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "6f5b6b52-c1aa-4148-880d-b9f41f1fdaa8",
            "port": "7e07d449-6475-4839-b43e-8aead8be2aac"
          },
          "target": {
            "block": "1441dc7a-d30b-4f4c-97e0-33ba979c192a",
            "port": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2"
          },
          "vertices": [
            {
              "x": -536,
              "y": 272
            }
          ]
        },
        {
          "source": {
            "block": "1441dc7a-d30b-4f4c-97e0-33ba979c192a",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "6ad76801-b927-48bc-a416-f170888affca",
            "port": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2"
          }
        },
        {
          "source": {
            "block": "6ad76801-b927-48bc-a416-f170888affca",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "21bad408-76bc-4c00-9d44-1110f1e11410",
            "port": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2"
          }
        },
        {
          "source": {
            "block": "21bad408-76bc-4c00-9d44-1110f1e11410",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "febb5024-8706-4dd6-a645-27945e6ae082",
            "port": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2"
          }
        },
        {
          "source": {
            "block": "febb5024-8706-4dd6-a645-27945e6ae082",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "bddf1f80-656c-4d50-9fc2-08652f6c5794",
            "port": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2"
          }
        },
        {
          "source": {
            "block": "bddf1f80-656c-4d50-9fc2-08652f6c5794",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "9478ff14-6e19-42b9-8033-d9aab0312701",
            "port": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2"
          }
        },
        {
          "source": {
            "block": "9478ff14-6e19-42b9-8033-d9aab0312701",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "af952fcf-95a9-4bfe-901a-5744fce54d91",
            "port": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2"
          }
        },
        {
          "source": {
            "block": "af952fcf-95a9-4bfe-901a-5744fce54d91",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "a7"
          }
        },
        {
          "source": {
            "block": "9478ff14-6e19-42b9-8033-d9aab0312701",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "a6"
          }
        },
        {
          "source": {
            "block": "bddf1f80-656c-4d50-9fc2-08652f6c5794",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "a5"
          },
          "vertices": [
            {
              "x": -328,
              "y": 368
            }
          ]
        },
        {
          "source": {
            "block": "febb5024-8706-4dd6-a645-27945e6ae082",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "a4"
          },
          "vertices": [
            {
              "x": -336,
              "y": 344
            }
          ]
        },
        {
          "source": {
            "block": "6f5b6b52-c1aa-4148-880d-b9f41f1fdaa8",
            "port": "7e07d449-6475-4839-b43e-8aead8be2aac"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "a0"
          }
        },
        {
          "source": {
            "block": "6ad76801-b927-48bc-a416-f170888affca",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "a2"
          },
          "vertices": [
            {
              "x": -352,
              "y": 320
            }
          ]
        },
        {
          "source": {
            "block": "21bad408-76bc-4c00-9d44-1110f1e11410",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "a3"
          },
          "vertices": [
            {
              "x": -344,
              "y": 320
            }
          ]
        },
        {
          "source": {
            "block": "1441dc7a-d30b-4f4c-97e0-33ba979c192a",
            "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "a1"
          },
          "vertices": [
            {
              "x": -360,
              "y": 304
            }
          ]
        },
        {
          "source": {
            "block": "5aafa498-0063-41c6-961d-b11ecd312222",
            "port": "constant-out"
          },
          "target": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "N"
          }
        },
        {
          "source": {
            "block": "1edb8255-0871-4c81-8288-50d9d96b5f73",
            "port": "cmp"
          },
          "target": {
            "block": "74a6cbb9-5a65-4365-824c-c8d0be5cfab7",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "d8f7e180-2cbc-4790-b754-493f7414d28c",
            "port": "constant-out"
          },
          "target": {
            "block": "6f5b6b52-c1aa-4148-880d-b9f41f1fdaa8",
            "port": "de2d8a2d-7908-48a2-9e35-7763a45886e4"
          }
        },
        {
          "source": {
            "block": "64bb1c49-3c0e-47f6-a6c7-421ce5049e3b",
            "port": "out"
          },
          "target": {
            "block": "6f5b6b52-c1aa-4148-880d-b9f41f1fdaa8",
            "port": "e19c6f2f-5747-4ed1-87c8-748575f0cc10"
          }
        }
      ]
    }
  },
  "dependencies": {
    "6a50747141af6d1cfb3bb9d0093fb94862ff5a65": {
      "package": {
        "name": "PrescalerN",
        "version": "0.1",
        "description": "Parametric N-bits prescaler",
        "author": "Juan Gonzalez (Obijuan)",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "e19c6f2f-5747-4ed1-87c8-748575f0cc10",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": 0,
                "y": 256
              }
            },
            {
              "id": "7e07d449-6475-4839-b43e-8aead8be2aac",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 720,
                "y": 256
              }
            },
            {
              "id": "de2d8a2d-7908-48a2-9e35-7763a45886e4",
              "type": "basic.constant",
              "data": {
                "name": "N",
                "value": "22",
                "local": false
              },
              "position": {
                "x": 352,
                "y": 56
              }
            },
            {
              "id": "2330955f-5ce6-4d1c-8ee4-0a09a0349389",
              "type": "basic.code",
              "data": {
                "code": "//-- Number of bits of the prescaler\n//parameter N = 22;\n\n//-- divisor register\nreg [N-1:0] divcounter;\n\n//-- N bit counter\nalways @(posedge clk_in)\n  divcounter <= divcounter + 1;\n\n//-- Use the most significant bit as output\nassign clk_out = divcounter[N-1];",
                "params": [
                  {
                    "name": "N"
                  }
                ],
                "ports": {
                  "in": [
                    {
                      "name": "clk_in"
                    }
                  ],
                  "out": [
                    {
                      "name": "clk_out"
                    }
                  ]
                }
              },
              "position": {
                "x": 176,
                "y": 176
              },
              "size": {
                "width": 448,
                "height": 224
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "2330955f-5ce6-4d1c-8ee4-0a09a0349389",
                "port": "clk_out"
              },
              "target": {
                "block": "7e07d449-6475-4839-b43e-8aead8be2aac",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "e19c6f2f-5747-4ed1-87c8-748575f0cc10",
                "port": "out"
              },
              "target": {
                "block": "2330955f-5ce6-4d1c-8ee4-0a09a0349389",
                "port": "clk_in"
              }
            },
            {
              "source": {
                "block": "de2d8a2d-7908-48a2-9e35-7763a45886e4",
                "port": "constant-out"
              },
              "target": {
                "block": "2330955f-5ce6-4d1c-8ee4-0a09a0349389",
                "port": "N"
              }
            }
          ]
        }
      }
    },
    "82041acc0f349dd5f038765c7d783bbe4b20dc00": {
      "package": {
        "name": "Flip-flop T",
        "version": "1.0.0",
        "description": "Toggle flip-flop",
        "author": "Carlos Diaz",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%22-252%20400.9%2090%2040%22%3E%3Ctext%20style=%22line-height:125%25%22%20x=%22-227.932%22%20y=%22429.867%22%20font-weight=%22400%22%20font-size=%2224.601%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%3E%3Ctspan%20x=%22-227.932%22%20y=%22429.867%22%3ETFF%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": 160,
                "y": -80
              }
            },
            {
              "id": "ffdd9aa2-aea3-4aa9-8431-80e799226774",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 672,
                "y": -64
              }
            },
            {
              "id": "f8a48554-c608-4f6a-ad56-04c70e5baef3",
              "type": "70ff7f2a2f0ede80fc37c66a997650a1d0d0102b",
              "position": {
                "x": 352,
                "y": -64
              },
              "size": {
                "width": 96,
                "height": 64
              }
            },
            {
              "id": "6d1059f6-855e-429e-8cf2-848f56d2c993",
              "type": "32200dc0915d45d6ec035bcec61c8472f0cc7b88",
              "position": {
                "x": 496,
                "y": -16
              },
              "size": {
                "width": 96,
                "height": 64
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "f8a48554-c608-4f6a-ad56-04c70e5baef3",
                "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
              },
              "target": {
                "block": "ffdd9aa2-aea3-4aa9-8431-80e799226774",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "f8a48554-c608-4f6a-ad56-04c70e5baef3",
                "port": "ffdd9aa2-aea3-4aa9-8431-80e799226774"
              },
              "target": {
                "block": "6d1059f6-855e-429e-8cf2-848f56d2c993",
                "port": "18c2ebc7-5152-439c-9b3f-851c59bac834"
              }
            },
            {
              "source": {
                "block": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2",
                "port": "out"
              },
              "target": {
                "block": "f8a48554-c608-4f6a-ad56-04c70e5baef3",
                "port": "6855f64f-fa1c-4371-b2e1-a98970674a96"
              }
            },
            {
              "source": {
                "block": "6d1059f6-855e-429e-8cf2-848f56d2c993",
                "port": "664caf9e-5f40-4df4-800a-b626af702e62"
              },
              "target": {
                "block": "f8a48554-c608-4f6a-ad56-04c70e5baef3",
                "port": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2"
              },
              "vertices": [
                {
                  "x": 408,
                  "y": 64
                }
              ]
            }
          ]
        }
      }
    },
    "70ff7f2a2f0ede80fc37c66a997650a1d0d0102b": {
      "package": {
        "name": "Flip-flop D",
        "version": "1.0.0",
        "description": "Delay flip-flop",
        "author": "Carlos Diaz",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%22-252%20400.9%2090%2040%22%3E%3Ctext%20style=%22line-height:125%25%22%20x=%22-231.121%22%20y=%22429.867%22%20font-weight=%22400%22%20font-size=%2224.601%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%3E%3Ctspan%20x=%22-231.121%22%20y=%22429.867%22%3EDFF%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "e5222a6e-4717-4f08-99d7-7cde897060ca",
              "type": "basic.code",
              "data": {
                "code": "// D flip-flop\n\nreg q = 1'b0;\n\nalways @(posedge clk)\nbegin\n  q <= d;\nend\n\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "d"
                    }
                  ],
                  "out": [
                    {
                      "name": "q"
                    }
                  ]
                }
              },
              "position": {
                "x": 432,
                "y": 128
              },
              "size": {
                "width": 368,
                "height": 272
              }
            },
            {
              "id": "6855f64f-fa1c-4371-b2e1-a98970674a96",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": 232,
                "y": 168
              }
            },
            {
              "id": "ffdd9aa2-aea3-4aa9-8431-80e799226774",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 864,
                "y": 232
              }
            },
            {
              "id": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": false
              },
              "position": {
                "x": 232,
                "y": 304
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "b32a6101-5bd1-4bcf-ae5f-e569b958a6a2",
                "port": "out"
              },
              "target": {
                "block": "e5222a6e-4717-4f08-99d7-7cde897060ca",
                "port": "d"
              }
            },
            {
              "source": {
                "block": "6855f64f-fa1c-4371-b2e1-a98970674a96",
                "port": "out"
              },
              "target": {
                "block": "e5222a6e-4717-4f08-99d7-7cde897060ca",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "e5222a6e-4717-4f08-99d7-7cde897060ca",
                "port": "q"
              },
              "target": {
                "block": "ffdd9aa2-aea3-4aa9-8431-80e799226774",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "32200dc0915d45d6ec035bcec61c8472f0cc7b88": {
      "package": {
        "name": "NOT",
        "version": "1.0.0",
        "description": "NOT logic gate",
        "author": "Jesús Arroyo",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%2291.33%22%20height=%2245.752%22%20version=%221%22%3E%3Cpath%20d=%22M0%2020.446h27v2H0zM70.322%2020.447h15.3v2h-15.3z%22/%3E%3Cpath%20d=%22M66.05%2026.746c-2.9%200-5.3-2.4-5.3-5.3s2.4-5.3%205.3-5.3%205.3%202.4%205.3%205.3-2.4%205.3-5.3%205.3zm0-8.6c-1.8%200-3.3%201.5-3.3%203.3%200%201.8%201.5%203.3%203.3%203.3%201.8%200%203.3-1.5%203.3-3.3%200-1.8-1.5-3.3-3.3-3.3z%22/%3E%3Cpath%20d=%22M25.962%202.563l33.624%2018.883L25.962%2040.33V2.563z%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%223%22/%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "5365ed8c-e5db-4445-938f-8d689830ea5c",
              "type": "basic.code",
              "data": {
                "code": "// NOT logic gate\n\nassign c = ~ a;",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "a"
                    }
                  ],
                  "out": [
                    {
                      "name": "c"
                    }
                  ]
                }
              },
              "position": {
                "x": 256,
                "y": 48
              }
            },
            {
              "id": "18c2ebc7-5152-439c-9b3f-851c59bac834",
              "type": "basic.input",
              "data": {
                "name": ""
              },
              "position": {
                "x": 64,
                "y": 144
              }
            },
            {
              "id": "664caf9e-5f40-4df4-800a-b626af702e62",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 752,
                "y": 144
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "18c2ebc7-5152-439c-9b3f-851c59bac834",
                "port": "out"
              },
              "target": {
                "block": "5365ed8c-e5db-4445-938f-8d689830ea5c",
                "port": "a"
              }
            },
            {
              "source": {
                "block": "5365ed8c-e5db-4445-938f-8d689830ea5c",
                "port": "c"
              },
              "target": {
                "block": "664caf9e-5f40-4df4-800a-b626af702e62",
                "port": "in"
              }
            }
          ]
        }
      }
    }
  }
}