[[_TOC_]]

# Introduccion
Esta carpeta presenta algunos ejemplos introductorios para aprender a utilizar el software **iCEstudio** con la placa **EDU-CIAA-FPGA**

# Informacion adicional
Para un instructivo detallado acerca del uso e instalacion del software, leer [esta pagina de la wiki](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/iCEstudio)

# Listado de ejemplos
1. [Lógica Combinacional](01-Combinacional)
2. [Contador de 4 bits](02-Contador)
3. [Modulacion de Ancho de Pulsos (PWM)](03-PulsosModulados)
4. [Puerto Serie](04-PuertoSerie)
5. [Interfaz Web](05-LoveControls)
