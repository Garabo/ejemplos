[[_TOC_]]

# Descripción

Un ejemplo inicial sencillo para:
1. Familiarizarse con el software iCEstudio
2. Aprender a relacionar las entradas y salidas de la EDU-CIAA-FPGA con los bloques del diagrama
3. Probar la sintesis y descarga del hardware desarrollado a la placa

**Autor:** Ramiro A. Ghignone

# Diagrama en bloques

![Bloques](.images/Bloques.png)

El diseño consta de:
1. Dos entradas asociadas a los botones 1 y 2 de la EDU-CIAA-FPGA
2. Una compuerta XNOR
3. Una salida asociada al LED 0 de la EDU-CIAA-FPGA

La numeración de los botones y los LEDs se realiza siguiendo el siguiente pinout:

![pinout](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/uploads/3ff1f8cf55bd6958f036165278ee3f56/image.png)

# Cómo probar el ejemplo

1. Abrir **iCEstudio**
2. Abrir el archivo **1_EDU_CIAA_FPGA.ice**
3. Conectar la **EDU-CIAA-FPGA** a la PC
4. **Verificar** el diseño (usando **Ctrl+R** o el botón en la esquina inferior derecha)
5. **Sintetizar** el diseño (usando **Ctrl+B** o el botón en la esquina inferior derecha)
6. **Cargar** el diseño en la placa (usando **Ctrl+U** o el botón en la esquina inferior derecha)
7. Presionar los botones 1 y 2 de la placa y ver cómo se comporta el LED 0

# Yo te propongo...

1. Editar los bloques de entradas y salidas para que el LED 3 varíe con los botones 3 y 4
2. Borrar la compuerta XNOR y reemplazarla por una compuerta NAND.
3. Borrar el circuito anterior y armar un diseño de compuertas interconectadas para que la placa se comporte según la siguiente **Tabla de Verdad** (prestar atención a las entradas y salidas). ¿Cuántas LUT de la FPGA se consumen? (ir a "Device Resource Usage" después de sintetizar):

| BOTON 1 | BOTON 3 | LED 2 |
|---------|---------|-------|
|    0    |    0    |   1   |
|    1    |    0    |   1   |
|    0    |    1    |   0   |
|    1    |    1    |   1   |

(0 : No presionado o apagado - 1 : Presionado o encendido)

4. Borrar el circuito anterior y armar un diseño de compuertas interconectadas para que la placa se comporte según la siguiente **Tabla de Verdad** ¿Cuántas LUT de la FPGA se consumen? :

| BOTON 1 | BOTON 2 | BOTON 3 | LED 1 |
|---------|---------|---------|-------|
|    0    |    0    |    0    |   0   | 
|    1    |    0    |    0    |   1   | 
|    0    |    1    |    0    |   1   | 
|    1    |    1    |    0    |   0   | 
|    0    |    0    |    1    |   1   | 
|    1    |    0    |    1    |   0   | 
|    0    |    1    |    1    |   0   | 
|    1    |    1    |    1    |   1   | 

# Enlaces relacionados

 - [Descripción de circuitos combinacionales en VHDL](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/wikis/Descripcion-combinacional-en-VHDL)
 - [Descripción de circuitos combinacionales en Verilog](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/wikis/Descripcion-combinacional-en-Verilog)
 - [Descripción y simulación de una compuerta en VHDL](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/00-Compuerta_AND)

No te olvides de revisar el [repositorio de ejemplos](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base) para ver otros diseños combinacionales en VHDL.

 