-- Librerias de VUnit para VHDL
library vunit_lib;
context vunit_lib.vunit_context;

--Testbench libraries
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--Testbench entity
entity tb is
  -- VUnit necesita este string en TODOS los Testbench:
  generic (runner_cfg : string);
end tb;

--Testbench architecture
architecture test of tb is

--Minimal time step
constant TIME_DELTA : time:=10 ns;

--Testbench signals
signal input    : std_logic_vector(15 downto 0);
signal selector : std_logic_vector(2 downto 0);
signal output1  : std_logic_vector(7 downto 0);

--DUTs declaration
component MUX_2CH is

    generic(BITS_PER_CH : integer := 1);

    port(   ch0     : in  std_logic_vector(BITS_PER_CH-1 downto 0);
            ch1     : in  std_logic_vector(BITS_PER_CH-1 downto 0);
            sel     : in  std_logic;
            out_ch  : out std_logic_vector(BITS_PER_CH-1 downto 0)  );

end component;

--Begin tesbench
begin

--DUTs instantation
mux2 : MUX_2CH generic map(BITS_PER_CH=>8) 
               port map(ch0=>input(7 downto 0),
                        ch1=>input(15 downto 8),
                        sel=>selector(0),
                        out_ch=>output1 ) ;

    --Simulation process
    simulate: process
    begin

        -- Iniciar test VUnit
        test_runner_setup(runner, runner_cfg);

        --Initial state
        input <= (others=>'0');
        selector <= (others=>'0');

        wait for TIME_DELTA;
        
        --Input data
        input <= "0000111100110101";

        --Check outputs
        wait for TIME_DELTA;
        selector <= "001";
        wait for TIME_DELTA;
        selector <= "010";
        wait for TIME_DELTA;
        selector <= "011";
        wait for TIME_DELTA;
        selector <= "100";
        wait for TIME_DELTA;
        selector <= "101";
        wait for TIME_DELTA;
        selector <= "110";
        wait for TIME_DELTA;
        selector <= "111";  
        wait for TIME_DELTA;
        wait for TIME_DELTA; --100 nseg

        -- Detener test VUnit
        test_runner_cleanup(runner);

    end process simulate;


end architecture test;


