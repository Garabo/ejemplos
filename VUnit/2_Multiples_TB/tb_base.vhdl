-- Librerias de VUnit para VHDL
library vunit_lib;
context vunit_lib.vunit_context;

-- Entidad del Testbench
entity tb_base is
  -- VUnit necesita este string en TODOS los Testbench:
  generic (runner_cfg : string);
end entity;

-- Arquitectura del Testbench
architecture tb of tb_base is
begin
  main : process
  begin

    --Iniciar testbench
    test_runner_setup(runner, runner_cfg);

    --Contenido del testbench
    report "Hello world!";

    --Terminar testbench
    test_runner_cleanup(runner);
  end process;
end architecture;
