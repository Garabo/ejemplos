-- Librerias de VUnit para VHDL
library vunit_lib;
context vunit_lib.vunit_context;

-- Entidad del Testbench
entity tb_falla is
  -- VUnit necesita este string en TODOS los Testbench:
  generic (runner_cfg : string);
end entity;

-- Arquitectura del Testbench
architecture tb of tb_falla is
begin
  main : process
  begin

    --Iniciar testbench
    test_runner_setup(runner, runner_cfg);

    --Este testbench va a fallar
    assert false report "Testbench failed !!" severity error;

    --Terminar testbench
    test_runner_cleanup(runner);
  end process;
end architecture;
