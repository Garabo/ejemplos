# Acerca de este ejemplo

Este ejemplo muestra como VUnit puede detectar, correr y resumir multiples testbenches.

La creacion de suites de verificacion compuestas por baterias de pruebas sirve para testear 
por separado distintos aspectos, componentes o situaciones de un modulo. Por ejemplo, si 
desarrollamos un receptor UART, podemos desarrollar un testbench para probar su comportamiento
normal y otro para evaluar su comportamiento en caso de fallas en la transmision.

## Partes importantes del codigo

Los archivos **run.py** y **tb_base.vhdl** se mantienen respecto al caso anterior.
Sin embargo aparecen dos nuevos archivos basados en tb_base con leves diferencias:
 - **tb_falla.vhdl** : Contiene un _assert_ que genera un error
 - **tb_warn.vhdl**  : Contiene un _assert_ que genera un warning

Al ejecutar VUnit, solo 2 de los 3 tests pasaran las pruebas.

## Instrucciones de uso

Abrir un Terminal y ejecutar:
```bash
# Para una version Python 3.x, con x>=6:
python3.x run.py
```

Aparece un resumen con el resultado de las pruebas ejecutadas:
```bash
pass (P=2 S=0 F=1 T=3) lib.tb_warn.all (0.5 seconds)

==== Summary ============================
pass lib.tb_base.all  (0.5 seconds)
pass lib.tb_warn.all  (0.5 seconds)
fail lib.tb_falla.all (0.9 seconds)
=========================================
pass 2 of 3
fail 1 of 3
=========================================
Total time was 1.9 seconds
Elapsed time was 2.0 seconds
=========================================
Some failed!
```

Podemos ver cuantas pruebas pasaron y cuantas fallaron del conjunto.

